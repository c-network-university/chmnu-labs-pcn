#include "single_thread_tcp_server.h"


struct CursorPosition {
    int x;
    int y;
};


void move_mouse(int x, int y);

void handle_connection(SOCKET socket, sockaddr_in* addr) {
    char* str_in_addr = inet_ntoa(addr->sin_addr);
    printf("[%s]>>%s\n", str_in_addr, "Establish new connection");
    
    // Code
    CursorPosition position;

    while (true)
    {
        int count = recv(socket, (char*)&position, sizeof(position), 0);
        if (count <= 0) {
            break;
        }
        move_mouse(position.x, position.y);
    }
    // _____

    close_socket(socket);

    printf("[%s]>>%s", str_in_addr, "Close incomming connection\n");
}

void move_mouse(int x, int y)
{
    SetCursorPos(x, y);    
}